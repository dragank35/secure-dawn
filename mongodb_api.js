/**
 * @author Dragan
 */

//var mongoUri = "mongodb://heroku_app14978982:2mcme8dca61bsp0mnpj9c967or@ds029287.mongolab.com:29287/heroku_app14978982"; 
var mongoUri = "mongodb://dragang:123123@ds029287.mongolab.com:29287/heroku_app14978982";
//var mongoUri = "mongodb://localhost:27017/google_maps";

//connect to Mongo
function connectToMongoDb (cb) {
    var mongoClient = require('mongodb').MongoClient;
    mongoClient.connect(mongoUri,function(err,db){
       if(err){
          console.log('error connecting to db');
          cb(null);
       } 
       cb(db);
    });
}

//return all markers
function getMarkers(cb){
    connectToMongoDb(function(db){
        if(!db){
        cb(null);
        }
        var markers = new Array();
        var collection = db.collection('markers');
        collection.find({},{_id:0}).toArray(function(err,docs){
            if(err){
                console.log('error returning data');
                cb(null);
            }
            docs.forEach(function(item){
            markers.push(item);
        });
        db.close();
        cb(markers);
        });    
    });
}

//return all markers from specific city
function getMarkersFromCity(city,cb){
    connectToMongoDb(function(db){
        if(!db){
        cb(null);
        }
        var markers = new Array();
        var collection = db.collection('markers');
        collection.find({city:city},{_id:0}).toArray(function(err,docs){
            if(err){
                console.log('error from mongodb');
                cb(null);
            }
            docs.forEach(function(item){
               markers.push(item); 
            });
            db.close();
            cb(markers);
        });
    });
}

//return markers by type
function getMarkersByType(type,cb){
    connectToMongoDb(function(db){
        if(!db){
        cb(null);
        }
        var markers = new Array();
        var collection = db.collection('markers');
        collection.find({type:type},{_id:0}).toArray(function(err,docs){
            if(err){
                console.log('error from mongodb');
                cb(null);
            }
            docs.forEach(function(item){
               markers.push(item); 
            });
            db.close();
            cb(markers);
        });
    });
}

//get all distinct cities
function getDistinctCities(cb){
    connectToMongoDb(function(db){
        if(!db){
            cb(null);
        }
        var cities = [];
        var collection = db.collection('markers');
        collection.distinct('city',function(err,docs){
            if(err){
                console.log('error');
                cb(null);
            }
            db.close();
            cb(docs);    
        });
    });
}

//get all distinct types
function getDistinctTypes(cb){
    connectToMongoDb(function(db){
        if(!db){
            cb(null);
        }
        var cities = [];
        var collection = db.collection('markers');
        collection.distinct('type',function(err,docs){
            if(err){
                console.log('error');
                cb(null);
            }
            db.close();
            cb(docs);    
        });
    });   
}

exports.getMarkersByType = getMarkersByType;
exports.getMarkers = getMarkers;
exports.getMarkersFromCity = getMarkersFromCity;
exports.getDistinctCities = getDistinctCities;
exports.getDistinctTypes = getDistinctTypes;