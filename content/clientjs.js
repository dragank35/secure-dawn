	var xmlHttp = null;
	var map = null;
	var arrMarkers = [];
	var tobaccoArr = [];
	var marketArr = [];
	var i = 0;
	var selected = -1;
	var myLocation = null;
	var tobaccoFlag = true;
	var marketFlag = true;
	var markerCluster = null;
	var geoInfoBubble = null;
	var geoMarker = null;
	var circle = null;
	var destionation = null;
	var directionsDisplay;
	var directionsService = new google.maps.DirectionsService();
	var markerDivs = null;
	var nearestMarker = null;
	var isGeolocated = true;
	var geocoder = null;
	var center;

	google.maps.event.addDomListener(window, 'load', initialize);
	

  	function initialize() {
		geocoder = new google.maps.Geocoder();
		directionsDisplay = new google.maps.DirectionsRenderer();
  		resetCheckboxes();
		
		//var url = "http://localhost:3000/api/markers";
		var url = "http://secure-dawn-2500.herokuapp.com/api/markers";

		var selectedCity = document.getElementById("city").value;
		if(selectedCity !== "All"){
			url += "?city="+selectedCity;
		}
		
		clearHtml();
  		resetVars();

		xmlHttp = new XMLHttpRequest();
		xmlHttp.onreadystatechange = callback;
		xmlHttp.open("GET",url,true);
		xmlHttp.send(null);

		center = new google.maps.LatLng(41.664438,21.712988);
		var mapOptions = {
	        center: center,
	        zoom: 9,
	        mapTypeId: google.maps.MapTypeId.ROADMAP,
	        panControl: true,
			zoomControl: true,
			mapTypeControl: false,
			scaleControl: true,
			streetViewControl: true,
			overviewMapControl: false
        };

		map = new google.maps.Map(document.getElementById("map-canvas"),mapOptions);
		directionsDisplay.setMap(map);
      	directionsDisplay.setPanel(document.getElementById("directions"));
      	if(navigator.geolocation){
      		navigator.geolocation.getCurrentPosition(
  			function(position){
				center = new google.maps.LatLng(position.coords.latitude,position.coords.longitude);
		        myLocation = center;

				geoMarker = new google.maps.Marker({
					position: center,
					animation: google.maps.Animation.DROP,
					map: map,
					icon: "content/funny_man.png",
					title:"your location"
	  			});

		  		geoInfoBubble = new InfoBubble({
			      	map: map,
			      	content: "<div class='geoInfoBuble'>This is your current location. Choose destination from the map to get directions...</div>",
			  		shadowStyle: 0,
			        minWidth: 240,
			        minHeight: 100,
			        padding: 0,
			        backgroundColor: '#f9f9f9',
			        borderRadius: 4,
			        arrowSize: 10,
			        borderWidth: 1,
			        borderColor: '#000000',
			        disableAutoPan: false,
			        hideCloseButton: false,
			        arrowPosition: 30,
			        arrowStyle: 0
			    });

		  		geoInfoBubble.open(map,geoMarker);
		  		

				map.setCenter(center);
		        map.setZoom(16);
		        
		        var currLoc = document.getElementById("grey");
				geocoder.geocode({'latLng': myLocation}, function(results, status) {
			      	if (status == google.maps.GeocoderStatus.OK) {
			        	if (results[1]) {
			        		currLoc.innerHTML = results[0].formatted_address;
		          		}
					}
				});
				document.getElementById("city").disabled = true;
				document.getElementById("locations").style.display = "none";
				document.getElementById("directions").style.display = "block";
				document.getElementById("reset").disabled = true;
			},
      		function(error){
      			switch(error.code){
			  		case error.PERMISSION_DENIED:
			  		document.getElementById("radius").disabled = true;
			  		document.getElementById("nearest").disabled = true;
			  		document.getElementById("current").disabled = true;
			  		document.getElementById("inRadius").disabled = true;
			  		isGeolocated = false;
				  	break;
				    case error.POSITION_UNAVAILABLE:
			      	alert("Location information is unavailable.");
			      	break;
				    case error.TIMEOUT:
			      	alert("The request to get user location timed out.");
			      	break;
				    case error.UNKNOWN_ERROR:
			      	alert("An unknown error occurred.");
				  	break;
      			}	
      		});

      	}
      	else{
      		alert('geolocation not supported');
      	}

		function callback(){
			if ( xmlHttp.readyState == 4 && xmlHttp.status == 200 ){
				var jsonstring = xmlHttp.responseText;
				
				var jmarkers = JSON.parse(jsonstring);

				for(var i=0;i<jmarkers.length;i++){
					addMarker(jmarkers[i]);	
				}

				if(!isGeolocated){
					markerCluster = new MarkerClusterer(map, arrMarkers);
					google.maps.event.addListener(markerCluster, 'clusterclick', function(cluster) {	
			    	if(selected != -1){
				    		resetCssStyle(selected);	
				    	}
				    	dropAllMarkers(arrMarkers);
					});
				}

				populateHtml(arrMarkers);
			}
		}	
	}
    
    // clear right panel
	function clearHtml(){
		var loc =  document.getElementById("locations");
		loc.innerHTML = "";
	}

    // reset global variables
    function resetVars(){
        xmlHttp = null;
        map = null;
        arrMarkers = [];
        links = [];
        tobaccoArr = [];
        marketArr = [];
        i = 0;
        selected = -1;
    }


	// center map
	function centerMap(){
	    closeInfoWindows(arrMarkers);
	    var ltlng = new google.maps.LatLng(41.664438,21.712988);
        map.setCenter(ltlng);
        map.setZoom(4);
        map.setZoom(9);
		dropAllMarkers(arrMarkers);
		resetCssStyle(selected);
 	}

	// create and add marker to map
	function addMarker(jmarker){
		var imageUrl = jmarker.type+".png";

		var myLatLng = new google.maps.LatLng(jmarker.position.latitude,jmarker.position.longitude);
		var marker = new google.maps.Marker({
				position: myLatLng,
				animation: google.maps.Animation.DROP,
				map: map,
				icon: imageUrl,
				title: jmarker.title
	  		});
		attachInfoWindow(marker,map);
		arrMarkers.push(marker);
		if(jmarker.type == "tobacco"){
			tobaccoArr.push(marker);
		}
		else{
			marketArr.push(marker);
		}
		marker.city = jmarker.city;
		marker.type = jmarker.type;
		marker.j = i;
		var	distance = google.maps.geometry.spherical.computeDistanceBetween(center, marker.position);
		marker.distance = distance;
		i++;
	}
	
	// add markers to right panel
   	function populateHtml(markers){
        var loc =  document.getElementById("locations");
        for(var i = 0; i<markers.length; i++){
            var div = document.createElement('div');
            div.type = markers[i].type;
            div.className = 'marker';
            loc.appendChild(div);

            var img = document.createElement('img');
            img.src = markers[i].icon;
            div.appendChild(img);

            var titleDiv = document.createElement('div');
            titleDiv.className = 'title';

            var a = document.createElement('a');
            a.id = "marker" + markers[i].j;
            a.href = "javascript:myclick(" + markers[i].j + ");";
    		a.innerHTML = markers[i].title + " - " + capitaliseFirstLetter(markers[i].city);
            titleDiv.appendChild(a);

            div.appendChild(titleDiv);
        }
		markerDivs = document.getElementsByClassName("marker");
    }
	
	// drop animation
	function dropAllMarkers(arr){
        for(var i =0; i<arr.length;i++){
            arr[i].setAnimation(google.maps.Animation.DROP);
        }
    }


    // open infowindow
	function myclick(i) {
	/*	if(myLocation){
			  	var request = {
				    origin:myLocation,
				    destination:arrMarkers[i].position,
			      	travelMode: google.maps.DirectionsTravelMode.WALKING
			  	};
			  	directionsService.route(request, function(response, status) {
				    if (status == google.maps.DirectionsStatus.OK) {
				      	directionsDisplay.setDirections(response);
      					closeInfoWindows(arrMarkers);
					    arrMarkers[i].setAnimation(google.maps.Animation.DROP);
						if(selected != -1){
							resetCssStyle(selected);
						}
					    setCss(arrMarkers[i].j);
					    selected = i;
				    }
		  		});
		}   */	
		//else{

		    arrMarkers[i].setAnimation(google.maps.Animation.DROP);
			google.maps.event.trigger(arrMarkers[i], 'click');
	        map.setCenter(arrMarkers[i].getPosition());
	        map.setZoom(18);
		//}
    }
    
    // close opened infowindows
    function closeInfoWindows(arr){
        for(var i =0; i<arr.length;i++){
            arr[i].infowindow.close();
        }
    }	
	
    // infowindow for every marker 
  	function attachInfoWindow(marker,map){

  		var infoBubble = new InfoBubble({
	      	map: map,
	      	content: '<div class="infoBuble">'+marker.title+" <br>  <div class='text'>Phone: +389xxxxxx <br> Fax: 021xxxxx <br> email: shop@shop.com </div> <input class='directionsClass' type='submit' value='get directions' onclick='getDirections(this);' </div>",
	  		shadowStyle: 0,
	        minWidth: 240,
	        minHeight: 120,
	        padding: 0,
	        backgroundColor: '#f9f9f9',
	        borderRadius: 4,
	        arrowSize: 10,
	        borderWidth: 1,
	        borderColor: '#000000',
	        disableAutoPan: false,
	        hideCloseButton: false,
	        arrowPosition: 30,
	        arrowStyle: 0
        });

		marker.infowindow = infoBubble;
		google.maps.event.addListener(marker, 'click', function() {
			/*
			if(myLocation){
			  	var request = {
				    origin:myLocation,
				    destination:marker.position,
			      	travelMode: google.maps.DirectionsTravelMode.DRIVING
			  	};
			  	directionsService.route(request, function(response, status) {
				    if (status == google.maps.DirectionsStatus.OK) {
				      	directionsDisplay.setDirections(response);
      					scrollToSelected(marker.j);
      					closeInfoWindows(arrMarkers);
						if(selected != -1){
							resetCssStyle(selected);
						}
					    setCss(marker.j);
					    selected = marker.j;
				    }
		  		});
			}
			*/
			//else{
				if(!isGeolocated){
					setCss(marker.j);
					scrollToSelected(marker.j);
					if(selected != -1){
						resetCssStyle(selected);
						arrMarkers[selected].infowindow.close();
					}
				}
				else{
					if(selected != -1){
						arrMarkers[selected].infowindow.close();
					}
				}
				infoBubble.open(map,marker);
				selected = marker.j;
				
			//}
		});
  	}

  	//mark selected item
  	function setCss(i){
  		var elem = document.getElementById("marker"+i).parentNode.parentNode;
        elem.className = "";
        elem.className = "selected";
	}

	//clear css
    function resetCssStyle(i){
    	var elem = document.getElementById("marker"+i).parentNode.parentNode;
        elem.className = "";
        elem.className = "marker";
    }

    //first letter capitalized
	function capitaliseFirstLetter(string)
	{
	    return string.charAt(0).toUpperCase() + string.slice(1);
	}

	// tobacco checkbox clicked
	function tobaccoClicked(elem){
		var checked = elem.checked;
		if(checked){
			showMarkers(elem.value);
		}
		else{
			hideMarkers(elem.value);
		}
	}

	// market checkbox clicked
	function marketClicked(elem){
		var checked = elem.checked;
		if(checked){
			showMarkers(elem.value);
		}
		else{
			hideMarkers(elem.value);
		}
	}

	//scroll sidebar vertically 
	function scrollToSelected(i){
		var locations = document.getElementById("locations");
		locations.scrollTop = i*45;
	}

	function resetCheckboxes(){
		var boxes = document.getElementsByName("checkbox-type");
		for(var i = 0; i<boxes.length;i++){
			boxes[i].checked = true;
		}
		tobaccoFlag = true;
		marketFlag = true;
	}

	function hideMarkers(type){
		for(var i = 0;i < markerDivs.length; i++){
			if(markerDivs[i].type == type){
				markerDivs[i].style.display = "none";
			}
			if (arrMarkers[i].type == type) {
				arrMarkers[i].setVisible(false);
			};
		}
		if(arrMarkers[selected].type == type){
			arrMarkers[selected].infowindow.close();
			resetCssStyle(selected);
		}
	}

	function showMarkers(type){
		for(var i = 0;i < markerDivs.length; i++){
			if(markerDivs[i].type == type){
				markerDivs[i].style.display = "block";
			}
			if (arrMarkers[i].type == type) {
				arrMarkers[i].setVisible(true);
			};
		}
		if(type == "tobacco"){
			dropAllMarkers(tobaccoArr);
		}
		else{
			dropAllMarkers(marketArr);
		}
		if(arrMarkers[selected].type == type){
			resetCssStyle(selected);
		}
	}

	// geolocation
	function getDirections(elem){
		if(myLocation){
			var request = {
				    origin:myLocation,
				    destination:arrMarkers[selected].position,
			      	travelMode: google.maps.DirectionsTravelMode.WALKING
		  	};
		  	directionsService.route(request, function(response, status) {
			    if (status == google.maps.DirectionsStatus.OK) {
			    	document.getElementById("directions").innerHTML = "";
			      	directionsDisplay.setDirections(response);
			    }
  			});
		}
		else{
			alert('geolocation disabled');
		}
	}

	function radiusChanged(val){
		circle.set('radius', parseInt(val, 10));
		getInRadius(parseInt(val, 10));
	}

	function getInRadius(value){
		for(var i = 0; i<arrMarkers.length;i++){
			if(parseInt(arrMarkers[i].distance,10) < value){
				arrMarkers[i].setVisible(true);
			}
			else{
				if(arrMarkers[i].j != selected){
					arrMarkers[i].setVisible(false);
					arrMarkers[i].infowindow.close();
				}
			}
		}
	}

	function findMinDistance(){
		var min = arrMarkers[0];
		for(var i = 1; i< arrMarkers.length; i++){
			if(arrMarkers[i].distance < min.distance){
				min = arrMarkers[i];
			}
		}
		return min;
	}

	function getNearest(){
		
		nearestMarker = findMinDistance();

		var request = {
				    origin:myLocation,
				    destination:nearestMarker.position,
			      	travelMode: google.maps.DirectionsTravelMode.WALKING
	  	};
	  	directionsService.route(request, function(response, status) {
		    if (status == google.maps.DirectionsStatus.OK) {
		    	document.getElementById("directions").innerHTML = "";
		      	directionsDisplay.setDirections(response);
		    }
		});
		nearestMarker.setVisible(true);
		nearestMarker.setAnimation(google.maps.Animation.DROP);
		google.maps.event.trigger(nearestMarker, 'click');
	}

	function getCurrentLocation(){
		geoInfoBubble.open(map,geoMarker);
	}

	function getInRadiusClick(){
		getInRadius(200);
		var circleOptions = {
			strokeColor: '#1668ff',
			strokeOpacity: 0.5,
			strokeWeight: 2,
			fillColor: '#bfd5ff',
			fillOpacity: 0.20,
			map: map,
			center: myLocation,
			radius: 200
		};

		circle = new google.maps.Circle(circleOptions);
        
	}
