var port = process.env.PORT || 3000;
var http = require('http');
var fs = require('fs');
var url = require('url');
var path = require('path');
var db = require('./mongodb_api');
//var router = require('./router');

var cache = {};
function cachAndDeliver(f,cb){
    if(!cache[f]){
        fs.readFile(f,function(err,data){
            if(!err){
                console.log('putting '+f+' in cache...')
                cache[f] = {content:data};
            }
            console.log('serving '+f)
            cb(err,data);
        });
        return;
    }
    console.log('loading '+f+' from cache...');
    cb(null,cache[f].content);
}

var mimeTypes = {
    '.js':'text/javascript',
    '.html':'text/html',
    '.css':'text/css',
    '.ico':'image/x-icon'
};

var whitelist = [
'.html',
'.js',
'.css',
'.ico',
'.png'
];

http.createServer(function (req, res) {
    var lookup = path.basename(decodeURI(req.url)) || 'index.html';
    var f = 'content/' + lookup;
    var ext = path.extname(lookup);

    var querystring = url.parse(req.url,true);
    var params = querystring.query;
    var pathname = querystring.pathname;
        
    if(pathname == "/api/markers"){    
        if(params.city == null){
            db.getMarkers(function(items){
                if(items){
                    res.writeHead(200,{'content-type':'application/json','Access-Control-Allow-Origin' : '*'});
                    res.end(JSON.stringify(items));        
                }
                else{
                    console.log('nothing returned....');
                }
            });
        }
        else{
            db.getMarkersFromCity(params.city,function(items){
                if(items){
                    res.writeHead(200,{'content-type':'application/json','Access-Control-Allow-Origin' : '*'});
                    res.end(JSON.stringify(items));        
                }
                else{
                    console.log('nothing returned....');
                }
            });
        }  
    }
    else
        if(pathname == "/api/cities"){
            db.getDistinctCities(function(items){
                if(items){
                    res.writeHead(200,{'content-type':'application/json','Access-Control-Allow-Origin' : '*'});
                    res.end(JSON.stringify(items));        
                }
            });
        }
    else
        if(pathname == "/api/types"){
            db.getDistinctTypes(function(items){
                if(items){
                    res.writeHead(200,{'content-type':'application/json','Access-Control-Allow-Origin' : '*'});
                    res.end(JSON.stringify(items));        
                }
            });
        }
    else{
        if (whitelist.indexOf(ext) === -1) {
        res.writeHead(500);
        res.end('Not Allowed!');
        return;
        }

        fs.exists(f, function (exists) {
            if (exists) {
                cachAndDeliver(f, function (err, data) {
                    if (err) {
                        res.writeHead(500);
                        res.end('Server Error!');
                        return;
                    }
                    var headers = { 'content-type': mimeTypes[path.extname(lookup)] };
                    res.writeHead(200, headers);
                    res.end(data);
                });
                return;
            }
            res.writeHead(404);
            res.end('Page Not Found!');
        });    
    }
    
    
}).listen(port);
console.log('server started...');